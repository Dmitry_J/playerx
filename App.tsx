import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { RouteNavigate } from './src/navigations/Routes';

const Stack = createStackNavigator();

function App() {
  return (
    <RouteNavigate />
  );
}

export default App;