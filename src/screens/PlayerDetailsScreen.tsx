import React from 'react';
import { View, 
    StyleSheet, 
    ScrollView, 
    StyleProp, 
    ViewStyle,
    TextStyle
} from 'react-native';
import { Header } from '../ui/Header';
import { IconButton } from '../ui/IconButton';
import { BackIcon } from '../assets/svg/BackIcon';
import { HandsIcon } from '../assets/svg/HandsIcon';
import { AppText } from '../ui/AppText';
import { color } from '../styles/color';

import { Statistics } from '../components/Statistics';
import { Members } from '../components/Members';
import { Comments } from '../components/Comments';

import { dataPlayers } from '../dataBase';

import { useNavigation, useRoute } from "@react-navigation/native";

export const PlayerDetailsScreen = () => {

    const navigation = useNavigation();
    const route = useRoute();

    const {playerId} = route.params;
    let title: string = '';
    let playerInfo = {}

    dataPlayers.forEach(item => {
        if (item.id === playerId) {
            title = item.title
            playerInfo = item;
        }
    })
    
    return (
        <>
            <Header containerStyles={headerStyles}>
                <View style={styles.wrapper}>
                    <IconButton 
                        containerStyles={iconButtonContainer} 
                        onPress={() => navigation.goBack()}
                    >
                        <BackIcon />
                    </IconButton>
                    <HandsIcon />
                </View>
                <AppText 
                    text={title}
                    colorText={color.white}
                    containerStyles={textStyles}
                />
            </Header>

            <ScrollView style={styles.playerContainer}>
                <Statistics playerInfo={playerInfo}/>
                <Members playerInfo={playerInfo}/>
                <Comments playerInfo={playerInfo}/>
            </ScrollView>
        </>
    )
}

const textStyles: StyleProp<TextStyle> = {
    fontSize: 17,
    lineHeight: 27
}

const headerStyles: StyleProp<ViewStyle> = {
    paddingVertical: 24,
    paddingHorizontal: 15,
    backgroundColor: '#bfb393', 
    alignItems: 'flex-start'
}

const iconButtonContainer: StyleProp<ViewStyle> = {
    position: 'relative',
    top: 0,
    right: 0
}

const styles = StyleSheet.create({
    wrapper: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 18
    },
    playerContainer: {
        flex: 1,
        backgroundColor: '#FFF',
    },
});