import React from 'react';
import { TabRoute } from '../navigations/TabRoute';
import { Header } from '../ui/Header';
import { IconButton } from '../ui/IconButton';
import { SettingIcon } from '../assets/svg/SettingIcon';
import { AppText } from '../ui/AppText';
import { StyleProp, ViewStyle } from 'react-native';
 
export const TodoScreen = () => {
    return (
        <>
            <Header containerStyles={headerStyles}>
                <AppText text="To do"/>
                <IconButton>
                    <SettingIcon />
                </IconButton>
            </Header>
            <TabRoute />
        </>
    )
}

const headerStyles: StyleProp<ViewStyle> = {
    paddingBottom: 10,
    borderBottomWidth: 0
}