import React, { useState } from "react";
import { Player } from "./Player";
import { StyleSheet, FlatList, View, Text, TouchableOpacity } from 'react-native';
import { Form, Field, FormProps } from 'react-final-form';
import { InputField } from "../ui/InputField";
import { SvgAddIcon } from "../assets/svg/SvgAddIcon";

import { dataPlayers } from "../dataBase";

export const SubscribedScreen = () => {

    const onSubmit = (values: FormProps) => {
        if (!values.player) return;
        // const newPlayer = {
        //     id: Date.now().toString(),
        //     title: values.player
        // }
        values.player = '';
    }

    return (
        <>
            <View style={styles.container}>
                <View>
                    <Form
                        onSubmit={onSubmit}
                        render={({ handleSubmit }) => (
                            <View style={styles.inputWrapper}>
                                <Field 
                                    name="player" 
                                    render={InputField} 
                                    placeholder="Add a prayer..." 
                                    placeholderTextColor="#9C9C9C"
                                    style={styles.inputBlock} 
                                />
                                <TouchableOpacity 
                                    style={styles.inputIcon}
                                    onPress={handleSubmit}
                                >
                                    <SvgAddIcon color="#72A8BC"/>
                                </TouchableOpacity>
                            </View>
                        )}
                    />
                    <FlatList 
                        style={{
                            marginTop: 15,
                            marginBottom: 20
                        }}
                        data={dataPlayers} 
                        renderItem={({item}) => !item.answered ? 
                            <Player answered={item.answered} title={item.title} info={item}/> :
                            <></>
                        }
                        keyExtractor={(item) => item.id}
                    />
                </View>
            </View>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 15,
        backgroundColor: '#fff',
    },
    inputWrapper: {
        position: 'relative',
        height: 50,
        width: '100%'
    },
    inputIcon: {
        position: 'absolute',
        top: 14,
        left: 15,
        zIndex: 10
    },
    inputBlock: {
        position: 'relative',
        paddingVertical: 15,
        paddingLeft: 52,
        paddingRight: 15,
        width: '100%',
        height: '100%',
        borderRadius: 10,
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: '#E5E5E5',
        fontSize: 17,
        lineHeight: 20,
        zIndex: 5,
    },
    itemContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        position: 'relative',
        height: 68,
        width: '100%',
        paddingLeft: 18,
        paddingVertical: 23,
        borderBottomWidth: 1,
        borderStyle: 'solid',
        borderColor: '#E5E5E5'
    },
    flexContainer: {
        flexDirection: 'row'
    },
    textPlayer: {
        fontSize: 17,
        lineHeight: 20,
        color: '#514D47',
        marginLeft: 15
    },
    iconContainer: {
        flexDirection: 'row',
        marginLeft: 20
    },
    textIcon: {
        marginLeft: 5
    }
})