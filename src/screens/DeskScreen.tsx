import React from 'react';
import { Container } from '../layout/Container';
import { Column } from '../components/Column';
import { Header } from '../ui/Header';
import { IconButton } from '../ui/IconButton';
import { AddIcon } from '../assets/svg/AddIcon';
import { AppText } from '../ui/AppText';

export const DeskScreen = ({ navigation }: any) => {

    const columnData = [
        {
            title: 'To do',
            id: '1'
        },
        {
            title: 'In Progress',
            id: '2'
        },
        {
            title: 'Completed',
            id: '3'
        },
    ]

    const handleGoTodo = (i: string) => {
        if (i === '1') navigation.navigate('Todo')
    }
    
    return (
        <>
            <Header>
                <AppText text="My Desk" />
                <IconButton>
                    <AddIcon color="#72A8BC" width='16' />
                </IconButton>
            </Header>
            <Container>
                {columnData.map((column) => 
                    <Column 
                        key={column.id} 
                        title={column.title} 
                        onPress={() => handleGoTodo(column.id)}
                    />
                )}
            </Container>
        </>
    )
}