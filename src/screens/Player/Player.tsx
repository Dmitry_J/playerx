import React, { useState } from "react";
import { 
    View, 
    StyleSheet, 
    TouchableOpacity, 
    TouchableNativeFeedback,
    StyleProp,
    ViewStyle,
    TextStyle
} from 'react-native';
import { Line } from "../../ui/Line";
import { CheckBox } from "../../ui/CheckBox";
import { UserIcon } from "../../assets/svg/UserIcon";
import { HandsIcon } from "../../assets/svg/HandsIcon";
import { useNavigation } from '@react-navigation/native';
import { IconButton } from "../../ui/IconButton";
import { AppText } from "../../ui/AppText";
import { color } from "../../styles/color";

const cutTitle = (title: string) => {
    let cutTitle: string = '';

    if(title.length > 18) {
        cutTitle = title.substring(0, 15) + '...';
    } else {
        cutTitle = title;
    }

    return cutTitle;
}

const Player: React.FC<PlayerItemProps> = ({ title, answered, info }) => {

    const [animateBlock, setAnimateBlock] = useState<boolean>(false);
    const navigation = useNavigation();
    
    const handleDeletePlayer = () => {
        setAnimateBlock(true);
        console.log('delete');
    }

    const handleCloseDeletePlayer = () => {
        setAnimateBlock(false);
        console.log('close');  
    }

    const handleGoDetail = () => {
        navigation.navigate('Player', {
            playerId: info.id
        });
    }

    return (
        <View style={{position: 'relative'}}>
            <TouchableNativeFeedback onLongPress={handleDeletePlayer} onPress={handleCloseDeletePlayer}>
                <View style={!animateBlock ? styles.itemContainer : styles.itemContainerTransform}>
                    <Line />
                    <View style={{flexDirection: 'row'}}>
                        <CheckBox checked={answered ? true : false}/>
                        <AppText text={cutTitle(title)} containerStyles={answered ? textThroughStyles : textStyles}/>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                        <View style={styles.iconContainer}>
                            <UserIcon />
                            <AppText text="3" containerStyles={textCountSubscribedStyles}/>
                        </View>
                        <TouchableOpacity style={styles.iconContainer} onPress={handleGoDetail}>
                            <HandsIcon color="#72A8BC"/>
                            <AppText text={info.timesPlayedTotal.toString()} containerStyles={timesPlayedTotalStyles}/>
                        </TouchableOpacity>
                    </View>
                </View>
            </TouchableNativeFeedback>
            <IconButton containerStyles={!animateBlock ? iconButtonContainerTransform : iconButtonContainer}>
                <AppText 
                    text="Delete" 
                    colorText={color.white} 
                    containerStyles={deleteButtonTextStyles}
                />
            </IconButton>
        </View>
    )
}

interface PlayerItemProps {
    title: string;
    id?: string;
    navigation?: any
    answered: boolean;
    info?: any
}

const textStyles: StyleProp<TextStyle> = {
    marginLeft: 15,
    textDecorationLine: 'none'
}

const textThroughStyles: StyleProp<TextStyle> = {
    marginLeft: 15,
    textDecorationLine: 'line-through',
}

const textCountSubscribedStyles: StyleProp<TextStyle> = {
    marginLeft: 5,
    fontSize: 12,
    lineHeight: 14
}

const timesPlayedTotalStyles: StyleProp<TextStyle> = {
    marginLeft: 5,
    fontSize: 12,
    lineHeight: 14,
}

const deleteButtonTextStyles: StyleProp<TextStyle> = {
    fontSize: 13,
    lineHeight: 15,
}

const iconButtonContainer: StyleProp<ViewStyle> = {
    display: 'flex',
    right: 0,
    top: 0,
    height: 68,
    width: 80,
    backgroundColor: '#AC5253',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 20
}

const iconButtonContainerTransform: StyleProp<ViewStyle> = {
    display: 'flex',
    right: -81,
    top: 0,
    height: 68,
    width: 80,
    backgroundColor: '#AC5253',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 20
}

const styles = StyleSheet.create({
    itemContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        position: 'relative',
        height: 68,
        width: '100%',
        paddingHorizontal: 18,
        paddingVertical: 23,
        borderBottomWidth: 1,
        borderStyle: 'solid',
        borderColor: '#E5E5E5',
        zIndex: 1
    },
    itemContainerTransform : {
        flexDirection: 'row',
        justifyContent: 'space-between',
        position: 'relative',
        height: 68,
        width: '100%',
        paddingHorizontal: 18,
        paddingVertical: 23,
        borderBottomWidth: 1,
        borderStyle: 'solid',
        borderColor: '#E5E5E5',
        transform: [
            {translateX: -80}
        ],
        zIndex: 1
    },
    iconContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 20
    }
})

export default Player;