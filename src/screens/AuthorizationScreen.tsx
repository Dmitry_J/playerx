import * as React from 'react';
import { View, StyleSheet, Alert } from 'react-native';
import { AppText } from '../ui/AppText';
import { Container } from '../layout/Container';
import { Form, Field, FormProps } from 'react-final-form';
import { InputField } from '../ui/InputField';
import { AppButton } from '../ui/AppButton';
import { color } from '../styles/color';

export const AuthorizationScreen = ({ navigation }: any) => {

  const onSubmit = (values: FormProps) => {
    if(values.email && values.password) {
      values.email = '';
      values.password = '';
      navigation.navigate('Desk');
    } else {
      Alert.alert('Error', 'Incorrect values', [
        {
          text: 'Ok',
        }
      ])
    }
  }

  return (
    <Container>
      <View style={styles.wrapper}>
        <AppText text="Autorization" />
        <Form
            onSubmit={onSubmit}
            render={({ handleSubmit }) => (
                <View>
                    {/* <Field 
                        name="name" 
                        render={InputField} 
                        placeholder="Enter your name" 
                        placeholderTextColor="#9C9C9C"
                    /> */}
                    <Field 
                        name="email" 
                        render={InputField} 
                        placeholder="Enter your email" 
                        placeholderTextColor="#9C9C9C"
                    />
                    <Field 
                        name="password" 
                        render={InputField} 
                        placeholder="Enter your password" 
                        placeholderTextColor="#9C9C9C"
                    />
                    <AppButton onPress={handleSubmit}>
                      <AppText text="Ok" colorText={color.white}/>
                    </AppButton>
                </View>
            )}
          />
      </View>
    </Container>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})