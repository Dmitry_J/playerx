import React, { useState } from 'react';
import { 
    View, 
    StyleSheet, 
    FlatList,
    StyleProp,
    ViewStyle,
    TextStyle
} from 'react-native';
import { SvgAddIcon } from '../assets/svg/SvgAddIcon';
import { Form, Field, FormProps } from 'react-final-form';
import { InputField } from '../ui/InputField';
import { Player } from './Player';
import { AppButton } from '../ui/AppButton';
import { Container } from '../layout/Container';
import { IconButton } from '../ui/IconButton';
import { AppText } from '../ui/AppText';
import { color } from '../styles/color';

import { dataPlayers } from "../dataBase";

export const MyPlayersScreen = () => {

    const [isAnsweredPlayers, setIsAnsweredPlayers] = useState<boolean>(false);

    const onSubmit = (values: FormProps) => {
        if (!values.player) return;
        // const newPlayer = {
        //     id: Date.now().toString(),
        //     title: values.player
        // }
        values.player = '';
    }

    const handleShowAnsweredPlayers = () => setIsAnsweredPlayers(true);

    const handleHideAnsweredPrayers = () => setIsAnsweredPlayers(false);

    return (
        <>
            <Container containerStyles={{padding: 15}}>
                <View>
                    <Form
                        onSubmit={onSubmit}
                        render={({ handleSubmit }) => (
                            <View style={styles.inputWrapper}>
                                <Field 
                                    name="player" 
                                    render={InputField} 
                                    placeholder="Add a prayer..." 
                                    placeholderTextColor="#9C9C9C"
                                    style={styles.input} 
                                />
                                <IconButton 
                                    containerStyles={iconButtonContainer}
                                    onPress={handleSubmit}
                                >
                                    <SvgAddIcon color="#72A8BC"/>
                                </IconButton>
                            </View>
                        )}
                    />

                    <FlatList 
                        style={{
                            marginTop: 15,
                            marginBottom: 20
                        }}
                        data={dataPlayers} 
                        renderItem={({item}) => !item.answered ? 
                            <Player answered={item.answered} title={item.title} info={item} /> :
                            <></>
                        }
                        keyExtractor={(item) => item.id}
                    />

                    <View style={{
                        alignItems: 'center'
                    }}>
                        {!isAnsweredPlayers ? 
                            <AppButton onPress={handleShowAnsweredPlayers} >
                                <AppText 
                                    text="Show Answered Prayers" 
                                    colorText={color.white}
                                    containerStyles={textStyle}
                                />
                            </AppButton>
                             :
                            <AppButton onPress={handleHideAnsweredPrayers} >
                                <AppText 
                                    text="hide Answered Prayers" 
                                    colorText={color.white}
                                    containerStyles={textStyle}
                                />
                            </AppButton>
                        }
                    </View>

                    {isAnsweredPlayers &&         
                        <FlatList 
                            style={{
                                marginTop: 15,
                                marginBottom: 20
                            }}
                            data={dataPlayers} 
                            renderItem={({item}) => item.answered ? 
                                <Player answered={item.answered} title={item.title} info={item} /> :
                                <></>
                            }
                            keyExtractor={(item) => item.id}
                        />
                    }
                </View>
            </Container>
        </>
    )
}

const iconButtonContainer: StyleProp<ViewStyle> = {
    position: 'absolute',
    width: 24,
    height: 24,
    top: 14,
    left: 15,
    zIndex: 10
}

const textStyle: StyleProp<TextStyle> = {
    fontSize: 12,
    lineHeight: 14,
    textTransform: 'uppercase'
}

const styles = StyleSheet.create({
    inputWrapper: {
        position: 'relative',
        height: 50,
        width: '100%'
    },
    input: {
        position: 'relative',
        paddingVertical: 15,
        paddingLeft: 52,
        paddingRight: 15,
        width: '100%',
        height: '100%',
        borderRadius: 10,
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: '#E5E5E5',
        fontSize: 17,
        lineHeight: 20,
        zIndex: 5,
    }
})