import Svg, {Rect} from 'react-native-svg';

import React from 'react';

interface AddIcon {
  color: string;
  width?: string;
  height?: string;
}

export const AddIcon: React.FC<AddIcon> = ({color, width = '22', height = '22'}) => {
  return (
    <Svg width={width} height={height} viewBox="0 0 22 22" fill="none">
      <Rect x="10" width="2" height={height} rx="1" fill={color} />
      <Rect
        x={height}
        y="10"
        width="2"
        height={height}
        rx="1"
        transform="rotate(90 22 10)"
        fill={color}
      />
    </Svg>
  );
};
