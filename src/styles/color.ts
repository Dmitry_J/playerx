export const color = {
    black: '#514D47',
    white: '#FFF',
    lightBlue: '#72A8BC',
    gray: '#9C9C9C',
    lightGray: '#C8C8C8',
    lightBrown: '#BFB393'
}