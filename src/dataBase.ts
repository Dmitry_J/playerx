export const dataPlayers = [
    {
        id: '1',
        title: 'Prayer item one text text',
        answered: false,
        dateAdded: 'July 20 2017',
        timesPlayedTotal: 100,
        timesPlayedByMe: 63,
        timesPlayedByOthers: 60,
        members: [
            {
                id: '111',
                avatar: '../../assets/img/player2.png'
            },
            {
                id: '112',
                avatar: '../../assets/img/player1.png'
            }
        ],
        comments: [
            {
                id: '11',
                name: 'Anna Barber',
                comment: 'Hey, Hey!',
                avatar: '../../../assets/img/player3.png',
                lastTime: '2 days ago'
            },
            {
                id: '21',
                name: 'Hanna Barber',
                comment: 'Hi!',
                avatar: '../../../assets/img/player3.png',
                lastTime: '2 days ago'
            },
            {
                id: '31',
                name: 'Gloria Barber',
                comment: 'How you doing?',
                avatar: '../../../assets/img/player3.png',
                lastTime: '2 days ago'
            }
        ]
    },
    {
        id: '2',
        title: 'Prayer item two which is for my family to love God whole heartedly.',
        answered: false,
        dateAdded: 'July 25 2017',
        timesPlayedTotal: 123,
        timesPlayedByMe: 63,
        timesPlayedByOthers: 60,
        members: [
            {
                id: '121',
                avatar: '../../assets/img/player2.png'
            },
        ],
        comments: [
            {
                id: '12',
                name: 'Anna Barber',
                comment: 'Hey, Hey!',
                avatar: '../../../assets/img/player3.png',
                lastTime: '2 days ago'
            }
        ]
    },
    {
        id: '3',
        title: 'Prayer item three...',
        answered: true,
        dateAdded: 'July 25 2017',
        timesPlayedTotal: 110,
        timesPlayedByMe: 63,
        timesPlayedByOthers: 60,
        members: [
            {
                id: '131',
                avatar: '../../assets/img/player1.png'
            }
        ],
        comments: [
            {
                id: '33',
                name: 'Gloria Barber',
                comment: 'How you doing?',
                avatar: '../../../assets/img/player3.png',
                lastTime: '2 days ago'
            }
        ]
    }
]