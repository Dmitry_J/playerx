import React from 'react';
import { View, StyleSheet, ViewProps, StyleProp, ViewStyle } from 'react-native';
import { color } from '../styles/color';

export const Container: React.FC<ContainerProps> = ({ containerStyles = {}, children }) => {
    return (
        <View style={[styles.container, containerStyles]}>
            {children}
        </View>
    )
}

interface ContainerProps extends ViewProps {
    containerStyles?: StyleProp<ViewStyle>
}

const styles = StyleSheet.create({
    container: {
        position: 'relative',
        padding: 15,
        backgroundColor: color.white,
        flex: 1,
    }
})