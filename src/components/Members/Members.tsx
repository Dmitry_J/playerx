import React from "react";
import { View, StyleSheet, Image, StyleProp, ViewStyle, TextStyle } from 'react-native';
import { AddIcon } from "../../assets/svg/AddIcon";
import { AppText } from "../../ui/AppText";
import { IconButton } from "../../ui/IconButton";
import { color } from "../../styles/color";

const Members: React.FC<MembersProps> = ({ playerInfo }) => {
    return (
        <>
            <View style={styles.container}>
                <AppText 
                    text="Members" 
                    containerStyles={membersTitleStyles}
                    colorText={color.lightBlue}
                />
                <View style={styles.wrapper}>
                    {playerInfo.members.map((item: any) => {
                        return (
                            <View key={item.id} style={styles.imageContainer}>
                                <Image 
                                    style={{
                                        width: '100%',
                                        height: '100%',
                                        resizeMode: 'cover'
                                    }}
                                    source={require('../../assets/img/player2.png')}
                                />
                            </View>
                        )
                    })}
                    <IconButton containerStyles={iconButtonContainer} >
                        <AddIcon color="#FFF" />
                    </IconButton>
                </View>
            </View>
        </>
    )
}

interface MembersProps {
    playerInfo: any
}

const membersTitleStyles: StyleProp<TextStyle> = {
    fontSize: 13,
    lineHeight: 15,
    textTransform: 'uppercase',
    marginBottom: 15
}

const iconButtonContainer: StyleProp<ViewStyle> = {
    position: 'relative',
    top: 0,
    right: 0,
    width: 32,
    height: 32,
    borderRadius: 16,
    backgroundColor: '#BFB393',
    justifyContent: 'center',
    alignItems: 'center'
}

const styles = StyleSheet.create({
    container: {
        paddingTop: 20,
        paddingBottom: 30,
        paddingHorizontal: 15
    },
    wrapper: {
        flexDirection: 'row'
    },
    imageContainer:  {
        width: 32,
        height: 32,
        borderRadius: 16 ,
        overflow: 'hidden',
        marginRight: 8
    }
});

export default Members;