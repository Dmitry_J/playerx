import React from "react";
import { Form, Field, FormProps } from 'react-final-form';
import { InputField } from '../../ui/InputField';
import { CommentIcon } from '../../assets/svg/CommentIcon';
import { 
    View, 
    StyleSheet, 
    TouchableOpacity,
    StyleProp,
    TextStyle
} from 'react-native';
import { CommentList } from "./components/CommentList";
import { AppText } from "../../ui/AppText";
import { color } from "../../styles/color";

const Comments: React.FC<CommentsProps> = ({ playerInfo }) => {

    const onSubmit = (values: FormProps) => {
        console.log(values.comment); 
    }

    return (
        <View>
            <AppText 
                text="Comments" 
                containerStyles={commentTitleStyles}
                colorText={color.lightBlue}
            />
            
            <CommentList playerInfo={playerInfo}/>

            <Form
                onSubmit={onSubmit}
                render={({ handleSubmit }) => (
                    <View style={styles.inputWrapper}>
                        <Field 
                            name="comment" 
                            render={InputField} 
                            placeholder="Add a comment..." 
                            placeholderTextColor="#9C9C9C"
                            style={styles.inputBlock} 
                        />
                        <TouchableOpacity 
                            style={styles.inputIcon}
                            onPress={handleSubmit}
                        >
                            <CommentIcon />
                        </TouchableOpacity>
                    </View>
                )}
            />
        </View>
    )
}

interface CommentsProps {
    playerInfo: any
}

const commentTitleStyles: StyleProp<TextStyle> = {
    paddingLeft: 15,
    fontSize: 13,
    lineHeight: 15,
    textTransform: 'uppercase',
    marginBottom: 15
}

const styles = StyleSheet.create({
    inputWrapper: {
        position: 'relative',
        height: 50,
        width: '100%'
    },
    inputIcon: {
        position: 'absolute',
        top: 17,
        left: 16,
        zIndex: 10
    },
    inputBlock: {
        position: 'relative',
        paddingVertical: 15,
        paddingLeft: 50,
        paddingRight: 15,
        width: '100%',
        height: '100%',
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderColor: '#E5E5E5',
        fontSize: 17,
        lineHeight: 20,
        zIndex: 5,
    },
});

export default Comments;