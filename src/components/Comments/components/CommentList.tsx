import React from "react";
import { 
    View, 
    StyleSheet, 
    Image,
    StyleProp,
    TextStyle
 } from 'react-native';
import { AppText } from "../../../ui/AppText";
import { color } from "../../../styles/color";

export const CommentList: React.FC<CommentListProps> = ({ playerInfo }) => {
    return (
        <>
        {playerInfo.comments.map((item: any) => {
            return (
                <View style={styles.container} key={item.id}>
                    <View style={styles.imageContainer}>
                        <Image
                            style={{
                                width: '100%',
                                height: '100%',
                                resizeMode: 'cover'
                            }}
                            source={require('../../../assets/img/player3.png')}
                        />
                    </View>
                    <View>
                        <View style={styles.commentGroup}>
                            <AppText 
                                text={item.name} 
                                containerStyles={nameStyles}
                            />
                            <AppText 
                                text={item.lastTime} 
                                containerStyles={lastTimeStyles}
                                colorText={color.gray}
                            />
                        </View>
                        <AppText text={item.comment} />
                    </View>
                </View>
            )
        })}
        </>
    )
}

interface CommentListProps {
    playerInfo: any
}

const nameStyles: StyleProp<TextStyle> = {marginRight: 6}

const lastTimeStyles: StyleProp<TextStyle> = {fontSize: 13, lineHeight: 16}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 12,
        paddingVertical: 15,
        borderColor: '#E5E5E5',
        borderTopWidth: 1,
        flexDirection: 'row',
        alignItems: 'center'
    },
    imageContainer: {
        height: 50,
        width: 50,
        borderRadius: 25,
        overflow: 'hidden',
        marginRight: 12
    },
    commentGroup: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 2,
    },
});