import React from "react";
import { View, StyleSheet, StyleProp, TextStyle, ViewStyle } from 'react-native';
import { Line } from "../../ui/Line";
import { AppText } from "../../ui/AppText";
import { color } from "../../styles/color";

const Statistics: React.FC<StatisticsProps> = ({ playerInfo }) => {

    return (
        <>
            <View style={styles.lastTimeContainer}>
                <Line containerStyles={lineStyles}/>
                <AppText text="Last prayed 8 min ago"/>
            </View>
            <View style={styles.infoBlock}>
                <View style={styles.infoTop}>
                    <View style={styles.infoItemFirst}>
                        <AppText 
                            text={playerInfo.dateAdded}
                            containerStyles={dateAddedTextStyle}
                            colorText={color.lightBrown} 
                        />
                        <AppText 
                            text="Date Added" 
                            containerStyles={textStyles}
                        />
                        <AppText 
                            text="Opened for 4 days"
                            colorText={color.lightBlue}
                            containerStyles={textStyles} 
                        />
                    </View>
                    <View style={styles.infoItem}>
                        <AppText 
                            text={playerInfo.timesPlayedTotal} 
                            containerStyles={textInfoItemStyles}
                            colorText={color.lightBrown}
                        />
                        <AppText 
                            text="Times Prayed Total" 
                            containerStyles={textStyles}
                        />
                    </View>
                </View>
                <View style={styles.infoTop}>
                    <View style={styles.infoItem}>
                        <AppText 
                            text={playerInfo.timesPlayedByMe} 
                            containerStyles={textInfoItemStyles}
                            colorText={color.lightBrown}
                        />
                        <AppText 
                            text="Times Prayed by Me" 
                            containerStyles={textStyles}
                        />
                    </View>
                    <View style={styles.infoItem}>
                        <AppText 
                            text={playerInfo.timesPlayedByOthers} 
                            containerStyles={textInfoItemStyles}
                            colorText={color.lightBrown}
                        />
                        <AppText 
                            text="Times Prayed by Others" 
                            containerStyles={textStyles}
                        />
                    </View>
                </View>
            </View>
        </>
    )
}

interface StatisticsProps {
    playerInfo: any //исправить
}

const lineStyles: StyleProp<ViewStyle> = {
    top: 15,
    left: 15
}

const dateAddedTextStyle: StyleProp<TextStyle> = {
    fontSize: 22,
    lineHeight: 26,
    marginBottom: 6
}

const textStyles: StyleProp<TextStyle> = {
    fontSize: 13,
    lineHeight: 15,
}

const textInfoItemStyles: StyleProp<TextStyle> = {
    fontSize: 32,
    lineHeight: 37,
}

const styles = StyleSheet.create({
    lastTimeContainer: {
        position: 'relative',
        paddingVertical: 15,
        paddingLeft: 25,
        paddingRight: 15
    },
    infoBlock: {
        borderColor: '#E5E5E5',
        borderTopWidth: 1,
    },
    infoTop: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderColor: '#E5E5E5'
    },
    infoItemFirst: {
        flexBasis: '50%',
        borderColor: '#E5E5E5',
        borderRightWidth: 1,
        paddingTop: 26,
        paddingHorizontal: 12,
    },
    infoItem: {
        flexBasis: '50%',
        borderColor: '#E5E5E5',
        borderRightWidth: 1,
        paddingHorizontal: 12,
        paddingVertical: 26
    },
});

export default Statistics;