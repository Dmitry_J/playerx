import React from 'react';
import { TouchableOpacity, Text, StyleSheet, TouchableOpacityProps } from 'react-native'


const Column: React.FC<ColumnProps> = ({ title, ...outerProps }) => {
    return (
        <TouchableOpacity style={styles.container} {...outerProps}>
            <Text style={styles.text}>{title}</Text>
        </TouchableOpacity>
    )
}

interface ColumnProps extends TouchableOpacityProps {
    title: string;
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 15,
        paddingVertical: 20,
        borderStyle: 'solid',
        borderColor: '#E5E5E5',
        borderWidth: 1,
        borderRadius: 4,
        marginBottom: 10
    },
    text: {
        color: '#514D47',
        lineHeight: 20,
        fontSize: 17,
    }
})

export default Column;