import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { AuthorizationScreen } from '../screens/AuthorizationScreen';
import { DeskScreen } from '../screens/DeskScreen';
import { TodoScreen } from '../screens/TodoScreen';
import { PlayerDetailsScreen } from '../screens/PlayerDetailsScreen';

const Stack = createStackNavigator();

export const RouteNavigate: React.FC = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{headerShown: false}}>
                <Stack.Screen name="Authorization" component={AuthorizationScreen} options={{title: 'Authorization'}}/>
                <Stack.Screen name="Desk" component={DeskScreen} options={{title: 'My Desk'}}/>
                <Stack.Screen name="Todo" component={TodoScreen} options={{title: 'To Do'}}/>
                <Stack.Screen name="Player" component={PlayerDetailsScreen}/>
            </Stack.Navigator>
        </NavigationContainer>
    )
}