import React from "react";
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { MyPlayersScreen } from "../screens/MyPlayersScreen";
import { SubscribedScreen } from "../screens/SubscribedScreen";
import { CounterIcon } from "../ui/CounterIcon";
import { dataPlayers } from "../dataBase";

const Tab = createMaterialTopTabNavigator();

let countSubscribed: number = 0;

dataPlayers.forEach((item) => {
    if(item.answered) countSubscribed++;
})

export const TabRoute = () => {
    return (
        <Tab.Navigator initialRouteName="My prayers" screenOptions={{
            tabBarInactiveTintColor: '#C8C8C8',
            tabBarActiveTintColor: '#72A8BC',
            tabBarIndicatorStyle: {
                borderColor: '#72A8BC',
                borderWidth: 2
            },
            tabBarIndicatorContainerStyle: {
                borderBottomWidth: 0.5,
                borderColor: '#E5E5E5',
            }
        }}>
            <Tab.Screen name="My players" component={MyPlayersScreen}/>
            <Tab.Screen 
                name={"Subscribed"} 
                component={SubscribedScreen} 
                options={{
                    tabBarIcon: () => <CounterIcon count={countSubscribed} />,
                    tabBarIconStyle: {
                        position: 'absolute',
                        left: 48,
                        top: 6
                    }
                }}
            />
        </Tab.Navigator>
    )
}