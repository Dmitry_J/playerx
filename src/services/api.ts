import axios from "axios";

export const signIn = (data: any) => {
    return axios.post('https://prayer.herokuapp.com/auth/sign-in', {data});
}