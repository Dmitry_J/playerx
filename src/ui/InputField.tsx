import React from "react";
import { TextInput } from 'react-native';
import { NativeSyntheticEvent, TextInputFocusEventData } from "react-native";
import { FieldRenderProps } from "react-final-form";

export const InputField: React.FC<InputFieldType> = ({input, meta, ...outerProps}) => {
    return (
        <TextInput {...input} {...meta} {...outerProps} />
    )
}

interface InputFieldType extends FieldRenderProps<string>{
    onBlur: (e: NativeSyntheticEvent<TextInputFocusEventData>) => void,
    onFocus: (e: NativeSyntheticEvent<TextInputFocusEventData>) => void,
}