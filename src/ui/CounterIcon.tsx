import React from 'react';
import { AppText } from './AppText';
import { color } from '../styles/color';
import { View, StyleSheet, StyleProp, TextStyle } from 'react-native';

export const CounterIcon: React.FC<ContainerProps> = ({ count }) => {
    return (
        <View style={styles.container}>
            <AppText 
                text={count.toString()} 
                colorText={color.white}
                containerStyles={textStyles}
            />
        </View>
    )
}

interface ContainerProps{
    count: number;
}

const textStyles: StyleProp<TextStyle> = {
    fontSize: 9,
    lineHeight: 11
}

const styles = StyleSheet.create({
    container: {
        width: 16,
        height: 16,
        borderRadius: 8,
        backgroundColor: '#AC5253',
        alignItems: 'center',
        justifyContent: 'center'
    }
})