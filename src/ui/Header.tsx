import React from 'react';
import { View, StyleSheet, StyleProp, ViewStyle, ViewProps } from 'react-native';
import { color } from '../styles/color';

export const Header: React.FC<HeaderProps> = ({ containerStyles = {}, children }) => {
    return (
        <View style={[styles.container, containerStyles]}>
            {children}
        </View>
    )
}

interface HeaderProps extends ViewProps {
    containerStyles?: StyleProp<ViewStyle>
}

const styles = StyleSheet.create({
    container: {
        position: 'relative',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 22,
        backgroundColor: color.white,
        borderColor: '#E5E5E5',
        borderBottomWidth: 1
    }
})