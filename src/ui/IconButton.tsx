import React from 'react';
import { TouchableOpacityProps } from 'react-native';
import { TouchableOpacity, StyleProp, StyleSheet, ViewStyle } from 'react-native';

export const IconButton: React.FC<IconButtonProps> = ({ children, containerStyles = {}, ...outerProps }) => {
    return (
        <TouchableOpacity style={[styles.container, containerStyles]} {...outerProps}>
            {children}
        </TouchableOpacity>
    )
}

interface IconButtonProps extends TouchableOpacityProps {
    containerStyles?: StyleProp<ViewStyle>
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        top: 20,
        right: 15,
    }
})