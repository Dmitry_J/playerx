import React from "react";
import { 
    ViewProps, 
    View, 
    StyleSheet, 
    StyleProp, 
    ViewStyle 
} from 'react-native';

export const Line: React.FC<LineType> = ({ containerStyles = {}, ...outerProps }) => {
    return (
        <View style={[styles.container, containerStyles]} {...outerProps}/>
    )
}

interface LineType extends ViewProps {
    containerStyles?: StyleProp<ViewStyle>
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        left: 0,
        top: 23,
        width: 3,
        height: 22,
        borderRadius: 10,
        backgroundColor: '#AC5253'
    }
})