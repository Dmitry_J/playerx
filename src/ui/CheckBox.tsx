import React from "react";
import { ChekedIcon } from "../assets/svg/chekedIcon";
import { TouchableOpacity, StyleSheet } from 'react-native';
import { color } from "../styles/color";

export const CheckBox: React.FC<CheckboxType> = ({ checked }) => {
    return (
        <>
            {checked ? 
                <TouchableOpacity style={styles.container}>
                    <ChekedIcon />
                </TouchableOpacity> : 
                <TouchableOpacity style={styles.container}/>
            }
        </>
    )
}

interface CheckboxType {
    checked: boolean
}

const styles = StyleSheet.create({
    container: {
        width: 22,
        height: 22,
        borderColor: color.black,
        borderWidth: 1,
        borderRadius: 4,
    }
})