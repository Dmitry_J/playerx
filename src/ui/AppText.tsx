import React from 'react';
import { Text, StyleProp, TextStyle, StyleSheet, TextProps } from 'react-native';
import { color } from '../styles/color';

export const AppText: React.FC<AppTextProps> = ({ containerStyles = {}, text, colorText }) => {
    return (
        <>
            {colorText ?         
                <Text style={[styles.container, containerStyles, {color: colorText}]}>
                    {text}
                </Text> :
                <Text style={[styles.container, containerStyles]}>
                    {text}
                </Text>
            }
        </>
    )
}

interface AppTextProps extends TextProps{
    containerStyles?: StyleProp<TextStyle>;
    text: string
    colorText?: string
}

const styles = StyleSheet.create({
    container: {
        fontFamily: 'sans-serif',
        fontSize: 17,
        lineHeight: 20,
        color: color.black
    }
})