import React from "react";
import { TouchableOpacityProps } from 'react-native';
import { TouchableOpacity, StyleSheet, ViewStyle, StyleProp } from 'react-native';

export const AppButton: React.FC<ButtonProps> = ({ 
    containerStyles = {},  
    children,
    ...outerProps 
}): JSX.Element => {
    return (
        <TouchableOpacity style={[styles.container, containerStyles]} {...outerProps}>
            {children}
        </TouchableOpacity>
    )
}

interface ButtonProps extends TouchableOpacityProps {
    containerStyles?: StyleProp<ViewStyle>
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        paddingVertical: 8,
        paddingHorizontal: 17,
        backgroundColor: '#BFB393',
        alignItems: 'center',
        shadowColor: '#424E75',
        shadowOffset: {
            height: 0,
            width: 2
        },
        shadowRadius: 15,
        borderRadius: 15
    }
})