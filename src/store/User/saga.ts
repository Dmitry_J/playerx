import { put, takeEvery, call } from 'redux-saga/effects'; //put предлазначен для асинхронных actions
import { userSignIn, asyncUserSignIn } from './reducers';
import { signIn } from '../../services/api';


function* userWorker() {
    // const userData = yield call(signIn);
    yield put(userSignIn)
}

export function* userWatcher() {
    yield takeEvery(asyncUserSignIn, userWorker);
}