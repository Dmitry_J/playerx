import { createSlice } from "@reduxjs/toolkit";

const userSlice = createSlice({
    name: 'user',
    initialState: {
        id: 0,
        email: '',
        password: ''
    },
    reducers: {
        userSignIn(state, action) {
            state.email = action.payload.email;
            state.password = action.payload.password;
        }
    }
})

export default userSlice.reducer;
export const {userSignIn} = userSlice.actions;
export const asyncUserSignIn = 'ASYNC_USER_SIGN_IN';