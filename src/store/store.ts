import { configureStore, combineReducers } from "@reduxjs/toolkit";
import userReducer from './User/reducers';
import createSagaMiddleware from "@redux-saga/core";

import { userWatcher } from "./User/saga";

const sagaMiddleware = createSagaMiddleware()

const reducer = combineReducers({
    user: userReducer,
})

export const store = configureStore({
    reducer: reducer,
    middleware: [sagaMiddleware]
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;

sagaMiddleware.run(userWatcher);